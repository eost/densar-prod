#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:author:
    Romain Pestourie (romain.pestourie@protonmail.com)
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:copyright:
    Romain Pestourie (romain.pestourie@protonmail.com)
    Maxime Bes de Berc (mbesdeberc@unistra.fr)
"""
import argparse
from collections import OrderedDict
import json
import os
import subprocess
import sys
import utm


def extr_coor_map(map_file):
    """
    Convert the map into a GEOJSON file calling gdal.
    Then extract name and coordinates of the Points and return a
    dictionnary.
    """
    suffix = map_file.split('.')[-1]
    if (suffix.lower() != 'geojson'):
        map_json = map_file.replace(suffix, 'geojson')
        subprocess.run(['ogr2ogr', '-f', 'GeoJSON', map_json, map_file])
    else:
        map_json = map_file
    with open(map_json, 'r') as my_map:
        content_json = json.load(my_map)
    stations_coor = {}
    for feature in content_json['features']:
        try:
            full_name = feature['properties']['Name']
        except KeyError:
            full_name = feature['properties']['name']
        pos = feature['geometry']['coordinates']
        coordinates = {'lon': pos[0], 'lat': pos[1], 'alt': 0}
        if len(pos) == 3:
            coordinates['alt'] = pos[2]
        stations_coor[full_name] = coordinates
    stations_coor = sort_dict(stations_coor)
    return stations_coor


def sort_dict(stations_coor):
    """
    Order the dictionnary in the increasing order of the profile number and
    the station name
    """
    nomenclature_sta = list(stations_coor.keys())[0]
    if '-' in nomenclature_sta:
        sub_order = OrderedDict(sorted(stations_coor.items(),
                                       key=lambda t: t[0].split('-')[1]))
        stations_coor_sort = OrderedDict(sorted(sub_order.items(),
                                         key=lambda t: t[0].split('-')[0]))
    else:
        stations_coor_sort = OrderedDict(sorted(stations_coor.items(),
                                                key=lambda t: t[0]))
    return stations_coor_sort


def data_conversion(nodes_wgs84):
    """
    Transform a dictionnary containing the receivers name and their positions
    in a new dictionnary with same structure but positions are projected in UTM
    """
    nodes_utm = OrderedDict()
    nzone = None
    for name, coor in nodes_wgs84.items():
        lon, lat, alt = (coor['lon'], coor['lat'], coor['alt'])
        if nzone is None:
            lon_utm, lat_utm, nzone, _ = utm.from_latlon(lat, lon)
            print("UTM Zone Number: %s" % nzone)
        else:
            lon_utm, lat_utm, _, _ = utm.from_latlon(lat, lon,
                                                     force_zone_number=nzone)
        coor_utm = {'lon': lon_utm, 'lat': lat_utm, 'alt': alt}
        nodes_utm[name] = coor_utm
    return nodes_utm


def write_sps_file(nodes_utm, dir_output, name, kind='R'):
    """
    Write the sps file (either with .rps or .sps extension, with respect to
    kind parameter)
    """
    if kind != 'R' and kind != 'S':
        sys.exit("Bad record type, exiting")
    results = []
    for pro_sta, coor in nodes_utm.items():
        try:
            profile, sta = (pro_sta.split('X')[0], pro_sta.split('X')[1])
        except IndexError:
            profile = '1'
            sta = pro_sta
        lon_sta = '{:.2f}'.format(coor['lon'])
        lat_sta = '{:.2f}'.format(coor['lat'])
        alt_sta = '{:.0f}'.format(coor['alt'])
        # According to SEG_SPS v2.1
        line_name = profile.rjust(10)
        point_number = sta.rjust(10)
        point_index = '1'
        if kind == 'R':
            point_code = 'G1'
        elif kind == 'S':
            point_code = 'S1'
        static_correction = '{:<4}'.format("")
        point_depth = "0".rjust(4)
        seismic_datum = "0".rjust(4)
        uphole_time = '{:<2}'.format("")
        water_depth = '{:<6}'.format("")
        map_grid_easting = lon_sta.rjust(9)
        map_grid_northing = lat_sta.rjust(10)
        surface_elevation = alt_sta.rjust(6)
        day_time = '{:<9}'.format("")
        result = '{}{}{}  {}{}{}{}{}{}{}{}{}{}{}{}'.format(kind,
                                                           line_name,
                                                           point_number,
                                                           point_index,
                                                           point_code,
                                                           static_correction,
                                                           point_depth,
                                                           seismic_datum,
                                                           uphole_time,
                                                           water_depth,
                                                           map_grid_easting,
                                                           map_grid_northing,
                                                           surface_elevation,
                                                           day_time,
                                                           '\n')
        results.append(result)
    path_output = os.path.join(dir_output, name)
    if kind == 'R':
        path_output = "{}{}".format(path_output, '_UTM.rps')
    elif kind == 'S':
        path_output = "{}{}".format(path_output, '_UTM.sps')

    with open(path_output, 'w') as output:
        for line in results:
            output.write(line)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert a map file in a SPS\
                                     receiver file with UTM coordinates')
    parser.add_argument('map_file',
                        help='Path of map file in every format allowed bay \
                        ogr2ogr. Tested with .kml (Google Earth) and .shp \
                        (ESRI shape file).')
    parser.add_argument('dir_output', help='Directory output')
    parser.add_argument('exp_name', help='Experience name')
    parser.add_argument('-s', '--source', help='Create a source file',
                        action='store_true')
    args = parser.parse_args()
    map_file = args.map_file
    dir_output = args.dir_output
    exp_name = args.exp_name
    source_file = args.source
    nodes_wgs84 = extr_coor_map(map_file)
    nodes_utm = data_conversion(nodes_wgs84)
    if args.source is True:
        write_sps_file(nodes_utm, dir_output, exp_name, kind='S')
    else:
        write_sps_file(nodes_utm, dir_output, exp_name)
