#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

Usage:
    merge_sds <srcsds> <destsds> [--nproc=<nproc>]

Example:
    merge_sds sds/ ../RUN1/sds/

Options:
    -h --help           Show this screen.
    --version           Show version.
    <srcsds>            Path to source sds archive.
    <destsds>           Path to destination sds archive.
    --nproc=<nproc>   Set number of processes [default: 19].
"""

from docopt import docopt
import glob
from multiprocessing import Pool
import os
import subprocess
import sys


def __dataselect(src, dest):
    cmd = "dataselect +o %s %s" % (dest, src)
    print(cmd)
    res = subprocess.run(cmd.split(" "))
    if res.returncode != 0:
        print("[ERROR] %s" % cmd)


def __rsync(src, dest):
    cmd = "rsync %s %s" % (src, dest)
    print(cmd)
    res = subprocess.run(cmd.split(" "))
    if res.returncode != 0:
        print("[ERROR] %s" % cmd)


if __name__ == '__main__':
    args = docopt(__doc__, version='merge_sds 0.1')
    # Uncomment for debug
    print(args)

    if args['<srcsds>'][-1] != '/' or args['<destsds>'][-1] != '/':
        print("One of given sds path is not ending by '/'")
        sys.exit()

    src_pattern = "%s/*/*/*/*/*" % args['<srcsds>']
    files = glob.glob(src_pattern)

    pool = Pool(int(args['--nproc']))
    for file in files:
        dest_file = file.replace(args['<srcsds>'], args['<destsds>'])
        dest_dir, _ = os.path.split(dest_file)
        if not os.path.isdir(dest_dir):
            os.makedirs(dest_dir)
        if os.path.isfile(dest_file):
            pool.apply_async(__dataselect, args=(file, dest_file))
        else:
            pool.apply_async(__rsync, args=(file, dest_file))
    pool.close()
    pool.join()
