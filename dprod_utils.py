# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 13:49:44 2021

@author: maxime
"""
import csv
import glob
from obspy import read_inventory, UTCDateTime
from obspy.core.inventory.channel import Channel
from obspy.core.inventory.inventory import Inventory
from obspy.core.inventory.network import Network
from obspy.core.inventory.station import Station
from obspy.core.inventory.util import Latitude, Longitude
from pyproj import Transformer
import simplekml
import xml.etree.ElementTree as ET


def check_log(logfile, cmd):
    try:
        f = open(logfile, 'r')
    except FileNotFoundError:
        cmd_list = list()
    else:
        cmd_list = f.readlines()
        f.close()
    if cmd not in [cmds[:-1] for cmds in cmd_list]:
        return True
    else:
        return False


def deploy_2_csv(deploy_list, csv_path):
    print("Writing csv")
    with open(csv_path, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=['station',
                                                     'latitude',
                                                     'longitude',
                                                     'elevation',
                                                     'serial',
                                                     'starttime',
                                                     'endtime',
                                                     'lockduration'])
        writer.writeheader()
        for item in deploy_list:
            writer.writerow(item)


def deploy_2_fdsnxml(deploy_list, response, xml_path, orientation,
                     netcode='SS', stationxml=None):
    print("Writing fdsnxml")
    deploy_time = min([d['starttime'] for d in deploy_list])
    pickup_time = UTCDateTime(0)
    for item in deploy_list:
        if 'endtime' in item.keys():
            pickup_time = max(item['endtime'], pickup_time)
    if pickup_time.ns == 0:
        pickup_time = None

    # create dummy station and channel
    _dummy_sta = Station(code='DUMMY',
                         latitude=0, longitude=0, elevation=0,
                         start_date=deploy_time, end_date=pickup_time,
                         creation_date=deploy_time,
                         total_number_of_channels=0)
    sr = response.get_sampling_rates()
    sr = sr[max(sr.keys())]['output_sampling_rate']
    _dummy_chan = Channel(code='DP1', location_code='00',
                          latitude=0, longitude=0, elevation=0, depth=0,
                          azimuth=0, dip=0, sample_rate=sr,
                          start_date=deploy_time, end_date=pickup_time,
                          types=['CONTINUOUS', 'GEOPHYSICAL'],
                          response=response)

    # create network and add stations and channels
    if stationxml is None:
        net = Network(code=netcode, start_date=deploy_time,
                      end_date=pickup_time, total_number_of_stations=0)
        inv = Inventory(networks=[net, ])
    else:
        inv = read_inventory(stationxml)
        _inv = inv.select(network=netcode)
        if len(_inv.get_contents()['networks']) == 0:
            net = Network(code=netcode, start_date=deploy_time,
                          end_date=pickup_time, total_number_of_stations=0)
            inv.networks.append(net)
        else:
            inet = inv.networks.index(_inv[0])
            net = inv[inet]
            if net.end_date is not None and pickup_time is not None:
                net.end_date = max(pickup_time, net.end_date)
            net.total_number_of_stations = len(net.stations)
    for item in deploy_list:
        _inv = inv.select(network=net.code, station=item['station'])
        if len(_inv.get_contents()['stations']) == 0:
            _sta = _dummy_sta.copy()
            _sta.code = "%s" % item['station']
            _sta.latitude = Latitude(item['latitude'], datum='WGS84')
            _sta.longitude = Longitude(item['longitude'], datum='WGS84')
            if 'elevation' in item.keys():
                _sta.elevation = item['elevation']
            _sta.start_date = item['starttime']
            if 'endtime' in item.keys():
                _sta.end_date = item['endtime']
            index = -1
        else:
            _sta = _inv[0][0]
            if _sta.end_date is not None and pickup_time is not None:
                _sta.end_date = max(pickup_time, _sta.end_date)
            _sta_codes = [station.code for station in net.stations]
            index = _sta_codes.index(_sta.code)
        for comp in ['Z', 'N', 'E']:
            _chan = _dummy_chan.copy()
            _chan.code = _chan.code.replace('1', comp)
            _chan.latitude = Latitude(item['latitude'], datum='WGS84')
            _chan.longitude = Longitude(item['longitude'], datum='WGS84')
            if 'elevation' in item.keys():
                _chan.elevation = item['elevation']
            _chan.start_date = deploy_time
            _chan.end_date = pickup_time
            if orientation == 'real':
                if comp == 'Z':
                    _chan.dip = 90
                elif comp == 'E':
                    _chan.azimuth = 270
                elif comp == 'N':
                    _chan.azimuth = 180
            elif orientation == 'lb':
                if comp == 'Z':
                    _chan.dip = -90
                elif comp == 'E':
                    _chan.azimuth = 90
            if _chan.start_date < _sta.start_date:
                _sta.start_date = _chan.start_date
            _sta.channels.append(_chan)
            _sta.total_number_of_channels += 1
        if _sta.start_date < net.start_date:
            net.start_date = _sta.start_date
        if index == -1:
            net.stations.append(_sta)
            net.total_number_of_stations += 1
        else:
            net.stations[index] = _sta
    # write inventory in stationxml format
    inv.write(xml_path, "STATIONXML", validate=True)


def deploy_2_kml(deploy_list, kml_path):
    kml = simplekml.Kml()
    print("Writing kml")
    for item in deploy_list:
        kml.newpoint(name="%s" % item['station'],
                     coords=[(item['longitude'], item['latitude'])],
                     description=item['serial'])
    kml.save(kml_path)


def read_deploy_csv(deploy_dir):
    print("Reading deployment files in csv")
    # look at csv files in deploy_dir
    deploy_files = glob.glob("%s/*.csv" % deploy_dir)

    # parse all xml files in one list of dict
    deploy_list = []
    for _csvfile in deploy_files:
        with open(_csvfile, 'r') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                sta = {'station': "%02dX%02d" % (int(row['line']), int(row['point'])),
                       'latitude': row['latitude'], 'longitude': row['longitude'],
                       'serial': row['serial'], 'starttime': row['starttime']}
                deploy_list.append(sta)
                print(sta)
    print("%d stations were found in the CSV file." % len(deploy_list))
    return deploy_list


def read_deploy_xml(deploy_dir):
    print("Reading deployment files in xml")
    # look at xml files in deploy_dir
    deploy_files = glob.glob("%s/*.xml" % deploy_dir)

    # parse all xml files in one list of dict
    deploy_list = list()
    for xml in deploy_files:
        tree = ET.parse(xml)
        root = tree.getroot()
        for child in root:
            point = "%02d" % int(float(child.find('station_no').text))
            line = "%02d" % int(float(child.find('line_no').text))
            lat = float(child.find('dep_latitude').text)
            long = float(child.find('dep_longitude').text)
            starttime = UTCDateTime(child.find('deployment_time').text)
            sta = {'station': "%sX%s" % (line, point),
                   'latitude': lat,
                   'longitude': long,
                   'serial': child.find('ADSR_serial_no').text,
                   'starttime': starttime}
            sta_codes = [d['station'] for d in deploy_list]
            if sta['station'] in sta_codes:
                index = sta_codes.index(sta['station'])
                if sta['starttime'] > deploy_list[index]['starttime']:
                    deploy_list[index]['latitude'] = sta['latitude']
                    deploy_list[index]['longitude'] = sta['longitude']
                    deploy_list[index]['serial'] = sta['serial']
                    deploy_list[index]['starttime'] = sta['starttime']
                    print("Update station %s (#%s): lat %s; long %s" %
                          (sta['station'], sta['serial'],
                           sta['latitude'], sta['longitude']))
            else:
                deploy_list.append(sta)
                print("Add station %s (#%s): lat %s; long %s" %
                      (sta['station'], sta['serial'],
                       sta['latitude'], sta['longitude']))
    print("%d stations were found in deployment files." % len(deploy_list))
    return deploy_list


def read_sort_station_info(station_info_file):
    print("Reading station info file")
    pos_list = list()
    with open(station_info_file) as csvfile:
        csvreader = csv.reader(csvfile, delimiter=' ')
        for row in csvreader:
            pos = dict()
            pos['serial'] = row[0]
            pos['starttime'] = UTCDateTime(row[1])
            pos['endtime'] = UTCDateTime(row[2])
            pos['lockduration'] = pos['endtime'] - pos['starttime']
            pos['latitude'] = row[3]
            pos['longitude'] = row[4]
            pos['elevation'] = row[5]
            serials = [p['serial'] for p in pos_list]
            if pos['serial'] in serials:
                index = serials.index(row[0])
                previous = pos_list[index]
                if pos['lockduration'] > previous['lockduration']:
                    previous['latitude'] = pos['latitude']
                    previous['longitude'] = pos['longitude']
                    previous['elevation'] = pos['elevation']
                if pos['starttime'] < previous['starttime']:
                    previous['starttime'] = pos['starttime']
                if pos['endtime'] > previous['endtime']:
                    previous['endtime'] = pos['endtime']
            else:
                pos_list.append(pos)
    return pos_list


def read_dgps(dgps_file, epsg):
    print("Reading dgps file")
    if epsg != 4979:
        transformer = Transformer.from_crs(epsg, 4979)
    pos_list = list()
    with open(dgps_file) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)
        for row in reader:
            pos = dict()
            try:
                lat, long = transformer.transform(row[1], row[2])
            except NameError:
                lat = row[1]
                long = row[2]
            pos['station'] = row[0]
            pos['latitude'] = lat
            pos['longitude'] = long
            pos['elevation'] = row[3]
            pos_list.append(pos)
    return pos_list


def update_deploy_list(deploy_list, pos_list):
    if 'serial' in pos_list[0].keys():
        key = 'serial'
    elif 'station' in pos_list[0].keys():
        key = 'station'
    for sta in deploy_list:
        keys = [p[key] for p in pos_list]
        if sta[key] in keys:
            index = keys.index(sta[key])
            for label in pos_list[index]:
                sta[label] = pos_list[index][label]
            print("Update station %s (#%s): lat %s; long %s; elevation %sm" %
                          (sta['station'], sta['serial'], sta['latitude'],
                           sta['longitude'], sta['elevation']))
        else:
            "[WARNING] This serial is not in deploy, should be!!"
    return deploy_list
