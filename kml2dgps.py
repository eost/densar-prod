#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 10:17:41 2020

@author: maxime
"""
import argparse
from collections import OrderedDict
import csv
import json
from pyproj import Transformer
import subprocess

def extr_coor_kml(map_kml):
    """
    Convert the kml map into a GEOJSON file calling gdal.
    Then extract name and coordinates of the Points and return a
    dictionnary.
    """
    map_json = map_kml.replace('.kml', '.json')
    subprocess.run(['ogr2ogr', '-f', 'GeoJSON', map_json, map_kml])
    with open(map_json, 'r') as my_map:
        content_json = json.load(my_map)
    stations_coor = {}
    for feature in content_json['features']:
        full_name = feature['properties']['Name']
        pos = feature['geometry']['coordinates']
        coordinates = {'lon': pos[0], 'lat': pos[1], 'alt': pos[2]}
        stations_coor[full_name] = coordinates
    stations_coor = sort_dict(stations_coor)
    subprocess.run(['rm', map_json])
    return stations_coor


def sort_dict(stations_coor):
    """
    Order the dictionnary in the increasing order of the profile number and
    the station name
    """
    nomenclature_sta = list(stations_coor.keys())[0]
    if '-' in nomenclature_sta:
        sub_order = OrderedDict(sorted(stations_coor.items(),
                                       key=lambda t: t[0].split('-')[1]))
        stations_coor_sort = OrderedDict(sorted(sub_order.items(),
                                         key=lambda t: t[0].split('-')[0]))
    else:
        stations_coor_sort = OrderedDict(sorted(stations_coor.items(),
                                                key=lambda t: t[0]))
    return stations_coor_sort


def write_csv(args, odict):
    if args.epsg != 4979:
        transformer = Transformer.from_crs(4979, args.epsg)
    with open(args.map_csv, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=['station', 'X', 'Y', 'Z'])
        writer.writeheader()
        for name, coor in odict.items():
            try:
                X, Y = transformer.transform(coor['lat'], coor['lon'])
            except NameError:
                X = coor['lat']
                Y = coor['lon']
                
            Z = coor['alt']
            line = {'station': name, 'X': X, 'Y': Y, 'Z': Z}
            writer.writerow(line)
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert a kml map in a \
                                     dgps file needed for dataless')
    parser.add_argument('map_kml', help='path map in kml')
    parser.add_argument('map_csv', help='out path map in csv')
    parser.add_argument('--epsg', help='write coordinates in given projection',
                        default=4979, type=int)
    args = parser.parse_args()
    nodes_wgs84 = extr_coor_kml(args.map_kml)
    write_csv(args, nodes_wgs84)
    