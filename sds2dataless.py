#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

 Usage:
    deploy2dataless.py <sdsroot> <response> <starttime> <outputxml>
[--endtime=<endtime>] [--latitude=<lat>] [--longitude=<long>] \
[--elevation=<ele>] [--orientation=<or>] [--network=<net>] \
[--stationxml=<staxml>]

Options:
    -h --help           Show this screen.
    --version           Show version.
    <sdsroot>           Root path of sds archive.
    <response>          Yaml response file, to be run by cmp_response.
    <starttime>         Starttime used to fill in inventory, common to all \
channels.
    <outputxml>        Path to stationxml file to be written.
    --endtime=<endtime> Endtime used to fill in inventory, common to all \
channels [default:None].
    --latitude=<lat>    Latitude used to fill in inventory, common to all \
channels [default: 48.5799].
    --longitude=<long>  Longitude used to fill in inventory, common to all \
channels [default: 7.7633].
    --elevation=<ele>   Elevation used to fill in inventory, common to all \
channels [default: 135].
    --orientation=<or>  Set orientation: by default all components are \
supposed to have been inverted at export process, to be oriented like \
broadband sensors, otherwise set parameter to real [default: lb].
    --network=<net>     Set network to scan [default: XX].
    --stationxml=<staxml>   Path to stationxml to append to.

"""
from cmp_response_stages.cmp_response import cmp_response
from obspy.clients.filesystem.sds import Client
from obspy.core import UTCDateTime
from docopt import docopt

from dprod_utils import *


if __name__ == "__main__":
    args = docopt(__doc__, version='sds2dataless 1.0')
    # Uncomment for debug
    # print(args)

    client = Client(args['<sdsroot>'])

    deploy_list = list()
    for item in client.get_all_stations():
        if item[0] == args['--network']:
            deploy = {'station': item[1],
                      'latitude': args['--latitude'],
                      'longitude': args['--longitude'],
                      'elevation': args['--elevation'],
                      'starttime': UTCDateTime(args['<starttime>'])}
            if args['--endtime']is not None:
                deploy['endtime'] = UTCDateTime(args['--endtime'])
            deploy_list.append(deploy)

    # write deploy in fdsn xml
    response = cmp_response(args['<response>'])
    deploy_2_fdsnxml(deploy_list, response, args['<outputxml>'],
                     args['--orientation'], netcode=args['--network'],
                     stationxml=args['--stationxml'])
