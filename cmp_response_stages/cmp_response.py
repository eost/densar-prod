# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 06:15:35 2019

@author: maxime
"""
import numpy as np
from obspy.core.inventory.response import CoefficientsTypeResponseStage
from obspy.core.inventory.response import FIRResponseStage
from obspy.core.inventory.response import InstrumentSensitivity
from obspy.core.inventory.response import PolesZerosResponseStage
from obspy.core.inventory.response import Response
from obspy.core.inventory.response import ResponseStage
from obspy.signal.invsim import corn_freq_2_paz
import yaml


def cmp_response(path2yaml):
    current_sps = None
    response = Response()
    yaml_file = open(path2yaml, 'r')
    stages = yaml.load(yaml_file, yaml.SafeLoader)
    yaml_file.close()

    for stage in stages.keys():

        if 'gain' in stages[stage].keys():
            stage_gain = stages[stage]['gain']
        else:
            stage_gain = 1

        if stages[stage]['sequence_number'] == 1:
            response.instrument_sensitivity = InstrumentSensitivity(stage_gain,
                                                                    stages[stage]['gain_frequency'],
                                                                    stages[stage]['input_units'],
                                                                    'COUNTS')

        if stages[stage]['type'] == 'pz':
            paz_dict = corn_freq_2_paz(stages[stage]['cut_off_frequency'],
                                       damp=stages[stage]['damping'])
            pzrs = PolesZerosResponseStage(stages[stage]['sequence_number'],
                                           stages[stage]['gain'],
                                           stages[stage]['gain_frequency'],
                                           stages[stage]['input_units'],
                                           stages[stage]['output_units'],
                                           'LAPLACE (RADIANS/SECOND)',
                                           stages[stage]['gain_frequency'],
                                           paz_dict['zeros'],
                                           paz_dict['poles'])
            response.response_stages.append(pzrs)

        elif stages[stage]['type'] == 'response':
            rs = ResponseStage(stages[stage]['sequence_number'],
                               stages[stage]['gain'],
                               stages[stage]['gain_frequency'],
                               stages[stage]['input_units'],
                               stages[stage]['output_units'])
            response.response_stages.append(rs)

        elif stages[stage]['type'] == 'rc':
            pole = -1./(float(stages[stage]['R']) * float(stages[stage]['C']))
            norm_factor = np.abs(np.prod(2j*np.pi*
                                 float(stages[stage]['gain_frequency']) -
                                 pole))
            pzrs = PolesZerosResponseStage(stages[stage]['sequence_number'],
                                           stages[stage]['gain'],
                                           stages[stage]['gain_frequency'],
                                           stages[stage]['input_units'],
                                           stages[stage]['output_units'],
                                           'LAPLACE (RADIANS/SECOND)',
                                           stages[stage]['gain_frequency'],
                                           [],
                                           [pole],
                                           normalization_factor=norm_factor)
            response.response_stages.append(pzrs)

        elif stages[stage]['type'] == 'coefficients':
            if current_sps is None:
                current_sps = stages[stage]['sample_rate']
            ctrs = CoefficientsTypeResponseStage(stages[stage]['sequence_number'],
                                                 stages[stage]['gain'],
                                                 0,
                                                 stages[stage]['input_units'],
                                                 stages[stage]['output_units'],
                                                 'DIGITAL',
                                                 numerator=[],
                                                 denominator=[],
                                                 decimation_input_sample_rate=current_sps,
                                                 decimation_factor=1,
                                                 decimation_offset=0,
                                                 decimation_delay=0,
                                                 decimation_correction=0)
            response.response_stages.append(ctrs)

        elif stages[stage]['type'] == 'sinc':
            n = stages[stage]['decimation_factor']
            poles = np.array([])
            zeros = np.array([])
            for k in np.arange(1, n):
                k_root = np.cos(2*k*np.pi/n) + 1j*np.sin(2*k*np.pi/n)
                zeros = np.append(zeros, k_root*np.ones(5))
            pzrs = PolesZerosResponseStage(stages[stage]['sequence_number'],
                                           1,
                                           0,
                                           'COUNTS',
                                           'COUNTS',
                                           'DIGITAL (Z-TRANSFORM)',
                                           0,
                                           zeros,
                                           poles,
                                           normalization_factor=1./n**5,
                                           decimation_input_sample_rate=current_sps,
                                           decimation_factor=n,
                                           decimation_offset=0,
                                           decimation_delay=0,
                                           decimation_correction=0)
            response.response_stages.append(pzrs)
            current_sps /= n

        elif stages[stage]['type'] == 'fir':
            typ = stages[stage]['phase']
            fir = stages[stage][typ]
            if fir['symmetry'] == 'ODD':
                coeff = np.append(fir['coefficients'],
                                  fir['coefficients'][-2::-1])
            elif fir['symmetry'] == 'EVEN':
                coeff = np.append(fir['coefficients'],
                                  fir['coefficients'][-1::-1])
            elif fir['symmetry'] == 'NONE':
                coeff = fir['coefficients']
            coeff = np.divide(coeff, np.sum(coeff), dtype=np.float128)
            delay = coeff.argmax()/current_sps
            if typ == 'linear':
                corr = (coeff.size-1) / (2*current_sps)
            elif typ == 'minimum':
                corr = delay
            dec_factor = stages[stage]['decimation_factor']
            frs = FIRResponseStage(stages[stage]['sequence_number'],
                                   1,
                                   0,
                                   'COUNTS',
                                   'COUNTS',
                                   symmetry='NONE',
                                   coefficients=list(coeff),
                                   decimation_input_sample_rate=current_sps,
                                   decimation_factor=dec_factor,
                                   decimation_delay=delay,
                                   decimation_correction=corr,
                                   decimation_offset=0)
            response.response_stages.append(frs)
            current_sps /= stages[stage]['decimation_factor']

    response.recalculate_overall_sensitivity()
    return response


if __name__ == "__main__":
    response = cmp_response("ads1283-250-64-linear.yaml")
    print(response)
