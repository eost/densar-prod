#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

Usage:
    sdsmod.py <src_sds> <dest_sds> [--net=<net>] \
[--stapattern=<stapa>] [--stasub=<stasub>] [--locpattern=<locpa>] \
[--locsub=<locsub>] [--chapattern=<chapa>] [--chasub=<chasub>]

Options:
    -h --help                Show this screen.
    --version                Show version.
    <src_sds>                Source sds (ie. sds to modify)
    <dest_sds>               Path to modified sds.
    --net=<net>              Net code to apply to destination sds.
    --stapattern=<stapa>     Regex pattern to match with station code.
    --stasub=<stasub>        Substitute of pattern in station code. 
    --locpattern=<locpa>     Regex pattern to match with location code.
    --locsub=<locsub>        Substitute of pattern in location code. 
    --chapattern=<chapa>     Regex pattern to match with channel code.
    --chasub=<chasub>        Substitute of pattern in channel code. 
"""
from docopt import docopt
import glob
from multiprocessing.pool import ThreadPool
import os
import re
import shutil
import subprocess
import sys

from dprod_utils import check_log

def __chan2sds(file, sds, logfile):
    cmd = "dataselect -SDS {} {}".format(sds, file)
    if check_log(logfile, cmd):
        _cmd = "msi -S {}".format(file)
        res = subprocess.run(_cmd.split(" "), capture_output=True, text=True)
        msiout = re.split("\||\n", res.stdout)
        _net = msiout[2]
        _sta = msiout[3]
        _loc = msiout[4]
        _cha = msiout[5]
        start_year = msiout[6].split(',')[0]
        end_year = msiout[7].split(',')[0]
        _years = list()
        _years.append(start_year)
        if end_year not in _years:
            _years.append(end_year)
        for _year in _years:
            currentdir = os.path.join(sds, _year, _net, _sta,
                                      "{}.D".format(_cha))
            if os.path.isdir(currentdir):
                shutil.rmtree(currentdir)
        print(cmd)
        res = subprocess.run(cmd.split(" "))
        if res.returncode == 0:
            with open(logfile, 'a') as f:
                f.write("%s\n" % cmd)
    

def __chanmsmod(file, logfile, netcode=None,
                stapattern=None, stasub=None,
                locpattern=None, locsub=None,
                chapattern=None, chasub=None):
    """
    https://docs.python.org/3/howto/regex.html
    """
    _, filename = os.path.split(file)
    _, _sta, _loc, _cha = filename.split('.')
    cmd = "msmod -i"
    if netcode is not None:
        cmd = "{} --net {}".format(cmd, netcode)
    if stapattern is not None:
        sta = re.sub(stapattern, stasub, _sta)
        cmd = "{} --sta {}".format(cmd, sta)
    if locpattern is not None:
        loc = re.sub(locpattern, locsub, _loc)
        cmd = "{} --loc {}".format(cmd, loc)
    if chapattern is not None:
        cha = re.sub(chapattern, chasub, _cha)
        cmd = "{} --chan {}".format(cmd, cha)
    cmd = "{} {}".format(cmd, file)
    if check_log(logfile, cmd):
        print(cmd)
        res = subprocess.run(cmd.split(" "))
        if res.returncode == 0:
            with open(logfile, 'a') as f:
                f.write("%s\n" % cmd)


def __sds2chan(files, outdir, logfile):
    cmd = "dataselect -CHAN {}".format(outdir)
    for f in files:
        cmd = "{} {}".format(cmd, f)
    _, fname = os.path.split(f)
    _net, _sta, _loc, _cha, _q, _y, _jd = fname.split('.')
    outfile = os.path.join(outdir, "{}.{}.{}.{}".format(_net, _sta,
                                                        _loc, _cha))
    if check_log(logfile, cmd):
        if os.path.isfile(outfile):
            os.remove(outfile)
        print(cmd)
        res = subprocess.run(cmd.split(" "))
        if res.returncode == 0:
            with open(logfile, 'a') as f:
                f.write("%s\n" % cmd)


if __name__ == "__main__":
    args = docopt(__doc__, version='sdsmod v0.1')
    # Uncomment for debug
    # print(args)
    
    if not os.path.isdir(args['<src_sds>']):
        sys.exit("SDS source does not exist!")
    if not os.path.isdir('tmpchan'):
        os.mkdir('tmpchan')
    if not os.path.isdir(args['<dest_sds>']):
         os.mkdir(args['<dest_sds>'])

    # convert sds 2 file per chan
    pool = ThreadPool(os.cpu_count()-1)
    for root, dirs, files in os.walk(args['<src_sds>']):
        if len(files) > 0:
            files = [os.path.join(root, _file) for _file in files]
            pool.apply_async(__sds2chan,
                             args=(files, 'tmpchan', "sdsmod.log"))
    pool.close()
    pool.join()
    
    # run msmod on chan files
    pool = ThreadPool(os.cpu_count()-1)
    for file in glob.glob(os.path.join('tmpchan', "*")):
        pool.apply_async(__chanmsmod,
                         args=(file, "sdsmod.log", args['--net'],
                               args['--stapattern'], args['--stasub'],
                               args['--locpattern'], args['--locsub'],
                               args['--chapattern'], args['--chasub']))
    pool.close()
    pool.join()
    
    # dispatch file per chan into dest sds archive
    pool = ThreadPool(os.cpu_count()-1)
    for file in glob.glob(os.path.join('tmpchan', "*")):
        pool.apply_async(__chan2sds,
                         args=(file, args['<dest_sds>'], "sdsmod.log"))
    pool.close()
    pool.join()
            
    
    