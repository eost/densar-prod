#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

 Usage:
    deploy2dataless.py <prospect> <project> <response> \
[--station-info=<sinfo>] [--stationxml=<staxml>] [--dgps-file=<dgps>] \
[--epsg=<epsg>] [--starttime=<st>] [--endtime=<et>] [--orientation=<or>] \
[--network=<net>] [--type=<type>] [--root=<root>]

Options:
    -h --help           Show this screen.
    --version           Show version.
    <prospect>          Prospect name.
    <project>           Project name.
    <response>          Path to response yaml file.
    --station-info=<sinfo>  Update gnss positions from positions exported \
from SoloLite app.
    --stationxml=<staxml>   Use network code from a given station xml.
    --dgps-file=<dgps>  Update gnss positions from a custom csv file.
    --epsg=<epsg>       Datum (epsg code) in which dpgs positions are \
expressed. Default to WGS84 (3D) [default: 4979].
    --starttime=<st>    Give custom starttime. Starttime is normally \
calculated from deployment file.
    --endtime=<et>      Give custom endtime. Endtime is normally calculated \
from station-info file.
    --orientation=<or>  Set orientation: by default all components are \
supposed to have been inverted at export process, to be oriented like \
broadband sensors, otherwise set parameter to real [default: lb].
    --network=<net>     Set SEED network code [default: XX].
    --type=<type>	Set type of deployment files, xml from app or \
custom csv [default: xml].
    --root=<root>       Root path of SmartSolo products \
[default: /home/densar/F_DRIVE].
"""
from cmp_response_stages.cmp_response import cmp_response
from docopt import docopt
import os
import sys

from dprod_utils import *


if __name__ == "__main__":
    args = docopt(__doc__, version='deploy2dataless 1.1')
    # Uncomment for debug
    # print(args)

    fdtpath = "%s/FDT" % args['--root']
    deploy_dir = "%s/%s/%s/DEPLOY" % (fdtpath,
                                      args['<prospect>'],
                                      args['<project>'])
    output_dir = "%s/%s/%s/OUTPUT" % (fdtpath,
                                      args['<prospect>'],
                                      args['<project>'])

    # check if project path does exist
    if not os.path.isdir("%s/%s/%s" % (fdtpath,
                                       args['<prospect>'],
                                       args['<project>'])):
        print("[Error] Such a project does not exist, please check.")
        sys.exit()

    # create output dir if it does not exist
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    if args['--type'] == 'csv':
        deploy_list = read_deploy_csv(deploy_dir)
    else:
        deploy_list = read_deploy_xml(deploy_dir)

    # read station_info.txt previously exported from SoloLite
    if args['--station-info']:
        print()
        sinfo = args['--station-info']
        deploy_list = update_deploy_list(deploy_list,
                                         read_sort_station_info(sinfo))

    # read dgps file (custom csv) given by user
    if args['--dgps-file']:
        print()
        deploy_list = update_deploy_list(deploy_list,
                                         read_dgps(args['--dgps-file'],
                                                   int(args['--epsg'])))

    if args['--starttime']:
        print()
        print("Update w. custom starttime")
        for item in deploy_list:
            item['starttime'] = UTCDateTime(args['--starttime'])

    if args['--endtime']:
        print()
        print("Update w. custom endtime")
        for item in deploy_list:
            item['endtime'] = UTCDateTime(args['--endtime'])

    # write deploy in kml
    print()
    kml_path = "%s/deploy.kml" % output_dir
    deploy_2_kml(deploy_list, kml_path)

    # write deploy in csv
    csv_path = "%s/deploy.csv" % output_dir
    deploy_2_csv(deploy_list, csv_path)

    # write deply in fdsn xml
    fdsnxml_path = "%s/station.xml" % output_dir
    response = cmp_response(args['<response>'])
    deploy_2_fdsnxml(deploy_list, response, fdsnxml_path,
                     args['--orientation'], netcode=args['--network'],
                     stationxml=args['--stationxml'])
