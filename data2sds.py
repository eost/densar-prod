#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

Usage:
    data2sds.py <prospect> <project> [--net=<net>] [--filters=<filters>] \
[--firname=<firname>] [--nproc=<nproc>] [--type=<type>] [--root=<root>]

Options:
    -h --help           Show this screen.
    --version           Show version.
    <prospect>          Prospect name.
    <project>           Project name.
    --net=<net>         Set network code [default: XX].
    --filters=<filters> Path to yaml file containing filters. Activates a \
de-facto decimation with firname described in yaml file.
    --firname=<firname> Fir name as written in yaml file.
    --nproc=<nproc>     Set number of processes [default: 19].
    --type=<type>       Set type of deployment files, xml from app or \
custom csv [default: xml].
    --root=<root>       Root path of SmartSolo products \
[default: /home/densar/F_DRIVE].
"""
from docopt import docopt
import glob
from multiprocessing import Pool
import numpy as np
from obspy import read, UTCDateTime
import os
from scipy.signal import lfilter
import shutil
import subprocess
import time
import yaml

from dprod_utils import check_log, read_deploy_csv, read_deploy_xml


def __clean_sds(sdsroot, sta, comp):
    current_dir = "%s/*/*/%s/*%s.*" % (sdsroot, sta, comp)
    [shutil.rmtree(d) for d in glob.glob(current_dir)]


def __dataselect(out_dir, pattern, station, log):
    comp = os.path.split(pattern)[1].split('.')[0][-1]
    files = glob.glob(pattern)
    cmd = 'dataselect -Ps -SDS %s' % out_dir
    for file in files:
        cmd = "%s %s" % (cmd, file)
    if check_log(log, cmd):
        print(cmd)
        __clean_sds(out_dir, station, comp)
        res = subprocess.run(cmd.split(" "))
        if res.returncode == 0:
            with open(log, 'a') as f:
                f.write("%s\n" % cmd)


def __decimate(tr, taps, factor, delay):
    # Trim to suitable sample: usec 0 + delay,
    # to be sure to begin at usec 0
    start = tr.stats.starttime
    if start.microsecond > 0:
        start = tr.stats.starttime + 1
        start = start.replace(microsecond=0)
    start += int(delay)*tr.stats.delta
    tr.trim(start, tr.stats.endtime)
    # Filter data
    tr.data = lfilter(taps, 1.0, tr.data)  # filter normalized
    # Apply correction according to input sampling rate
    tr.stats.starttime -= delay*tr.stats.delta
    # Performs decimation
    tr.decimate(factor, no_filter=True)
    # Keep it in int32 for Steim1 compression
    tr.data = tr.data.astype(np.int32)


def __msmod(net, station, comp, file, log):
    cmd = 'msmod -i --net %s --sta %s --loc 00 --chan DP%c %s' % (net, station, comp,
                                                                  file)
    if check_log(log, cmd):
        print(cmd)
        res = subprocess.run(cmd.split(" "))
        if res.returncode == 0:
            with open(log, 'a') as f:
                f.write("%s\n" % cmd)


def __process_decimate(file, delay, factor, taps, outdir):
    cmd = "__process_decimate %s" % file
    # Check if file has already been treated successfully
    if check_log(logfile, cmd):
        try:
            print(cmd)
            st = read(file)
            for tr in st:
                # Decimate
                __decimate(tr, taps, factor, delay)
            # Trim data at midnight
            st.sort(keys=['starttime'])
            start = st[0].stats.starttime
            if start.hour == 23 and start.minute == 59:
                start += 86400
            start = start.replace(hour=0, minute=0, second=0, microsecond=0)
            st.trim(starttime=start, endtime=start+86400, nearest_sample=False)
            # Write data
            outfile = file.split('/')[-1]
            outpath = os.path.join(outdir, outfile)
            st.write(outpath, format="MSEED", encoding='STEIM1', reclen=4096,
                     byteorder='=')
        except Exception:
            pass
        else:
            with open(logfile, 'a') as f:
                f.write("%s\n" % cmd)


def __rewrite2cday(file, outdir, logfile):
    # Parse data from filename
    index = file.split('.')[1]
    year = file.split('.')[2]
    month = file.split('.')[3]
    day = file.split('.')[4]
    # Make list with filename, previous one and next one
    # TODO: parse with date -> what happens if gap?
    files2process = list()
    files2process.append(file)
    # previous file
    _dir, _ = os.path.split(file)
    _pattern = os.path.join(_dir, "%s.%s.*.%c.miniseed" % (serial,
                                                           int(index)-1,
                                                           comp))
    _otherfiles = glob.glob(_pattern)
    if len(_otherfiles) > 0:
        files2process.extend(_otherfiles)
    # next file
    _pattern = os.path.join(_dir, "%s.%s.*.%c.miniseed" % (serial,
                                                           int(index)+1,
                                                           comp))
    _otherfiles = glob.glob(_pattern)
    if len(_otherfiles) > 0:
        files2process.extend(_otherfiles)
    # Make command
    tstart = UTCDateTime("%s-%s-%s" % (year, month, day))
    t0 = tstart - 60
    t1 = tstart + 86400 + 60
    fdate = '%Y,%j,%H,%M,%S'
    outfile = file.replace(raw_dir, outdir)
    cmd = "dataselect -Ps -ts %s -te %s -o %s" %\
        (t0.strftime(fdate), t1.strftime(fdate), outfile)
    for _file in files2process:
        cmd = "%s %s" % (cmd, _file)
    # Run command if not written in log
    if check_log(logfile, cmd):
        print(cmd)
        try:
            os.remove(outfile)
        except FileNotFoundError:
            pass
        res = subprocess.run(cmd.split(" "))
        if res.returncode == 0:
            with open(logfile, 'a') as f:
                f.write("%s\n" % cmd)


if __name__ == "__main__":
    args = docopt(__doc__, version='data2sds 1.1')
    # Uncomment for debug
    # print(args)

    deploy_dir = "%s/FDT/%s/%s/DEPLOY" % (args['--root'],
                                          args['<prospect>'],
                                          args['<project>'])
    rootpath = "%s/SOLODATA/%s/%s" % (args['--root'],
                                      args['<prospect>'],
                                      args['<project>'])
    raw_dir = "%s/raw_mseed" % rootpath
    cday_dir = "%s/cday" % rootpath
    cdaydecimated_dir = "%s/cdaydec" % rootpath
    sds_dir = "%s/tmpsds" % rootpath
    logfile = "%s/data2sds.log" % rootpath

    os.makedirs(sds_dir, exist_ok=True)

    if args['--type'] == 'csv':
        deploy_list = read_deploy_csv(deploy_dir)
    else:
        deploy_list = read_deploy_xml(deploy_dir)

    ser_list = [d['serial'] for d in deploy_list]
    sta_list = [d['station'] for d in deploy_list]

    # change headers in raw files
    pool = Pool(int(args['--nproc']))
    for serial in ser_list:
        station = sta_list[ser_list.index(serial)]
        for comp in ['Z', 'N', 'E']:
            pattern = '%s/%s*%s.miniseed' % (raw_dir, serial, comp)
            files = glob.glob(pattern)
            if len(files) > 0:
                for file in files:
                    pool.apply_async(__msmod,
                                     args=(args['--net'], station, comp,
                                           file, logfile))
                    time.sleep(0.1)
            else:
                print("No available data for %s.%s" % (station, comp))
    pool.close()
    pool.join()

    if args['--filters']:
        os.makedirs(cday_dir, exist_ok=True)
        os.makedirs(cdaydecimated_dir, exist_ok=True)
        # Rewrite files with some data before and after
        for serial in ser_list:
            for comp in ['Z', 'N', 'E']:
                files = glob.glob('%s/%s.*.%c.miniseed' % (raw_dir, serial,
                                                           comp))
                for file in files:
                    __rewrite2cday(file, cday_dir, logfile)

        # Read fir in yaml file
        with open(args['--filters'], 'r') as yfile:
            firs = yaml.safe_load(yfile)

        # Select filter in file
        fir = firs[args['--firname']]
        # Build complete coefficients array according to symmetry
        if fir['symmetry'] == 'ODD':
            taps = np.append(fir['coefficients'], fir['coefficients'][-2::-1])
        elif fir['symmetry'] == 'EVEN':
            taps = np.append(fir['coefficients'], fir['coefficients'][-1::-1])
        else:
            taps = np.array(fir['coefficients'])

        # Normalize to 1 (just in case)
        taps = np.divide(taps, np.sum(taps))
        # Calc delay
        delay = (taps.size-1)/2  # To be divided by sps
        # Read factor
        factor = int(fir['factor'])

        # Decimate
        files = glob.glob(os.path.join(cday_dir, '*'))
        pool = Pool(int(args['--nproc']))
        for file in files:
            pool.apply_async(__process_decimate,
                             args=(file, delay, factor, taps,
                                   cdaydecimated_dir))
        pool.close()
        pool.join()
        raw_dir = cdaydecimated_dir

    # write records in sds
    pool = Pool(int(args['--nproc']))
    for serial in ser_list:
        station = sta_list[ser_list.index(serial)]
        for comp in ['Z', 'N', 'E']:
            pattern = '%s/%s*%s.miniseed' % (raw_dir, serial, comp)
            if len(glob.glob(pattern)) != 0:
                pool.apply_async(__dataselect,
                                 args=(sds_dir, pattern, station, logfile))
                time.sleep(0.1)
            else:
                print("No available data for %s.%s" % (station, comp))
    pool.close()
    pool.join()
