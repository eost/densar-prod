#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

 Usage:
    deploy2kml.py <prospect> <project> [--type=<type>] [--root=<root>]

Options:
    -h --help           Show this screen.
    --version           Show version.
    <prospect>          Prospect name.
    <project>           Project name.
    --type=<type>       Set type of deployment files, xml from app or custom \
csv [default: xml].
    --root=<root>       Root path of SmartSolo products \
[default: /home/densar/F_DRIVE].
"""
from docopt import docopt
import os
import sys

from dprod_utils import *


if __name__ == "__main__":
    args = docopt(__doc__, version='deploy2kml 1.0')
    # Uncomment for debug
    # print(args)

    fdtpath = "%s/FDT" % args['--root']
    deploy_dir = "%s/%s/%s/DEPLOY" % (fdtpath,
                                      args['<prospect>'],
                                      args['<project>'])
    output_dir = "%s/%s/%s/OUTPUT" % (fdtpath,
                                      args['<prospect>'],
                                      args['<project>'])

    # check if project path does exist
    if not os.path.isdir("%s/%s/%s" % (fdtpath,
                                       args['<prospect>'],
                                       args['<project>'])):
        print("[Error] Such a project does not exist, please check.")
        sys.exit()

    # create output dir if it does not exist
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    if args['--type'] == 'csv':
        deploy_list = read_deploy_csv(deploy_dir)
    else:
        deploy_list = read_deploy_xml(deploy_dir)

    # write deploy in kml
    print()
    kml_path = "%s/deploy.kml" % output_dir
    deploy_2_kml(deploy_list, kml_path)
